<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Logout</name>
   <tag></tag>
   <elementGuidId>cdf99a95-0f95-40a7-b9a6-5ae09f633a0c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class='btn btn-link text-decoration-none']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class='btn btn-link text-decoration-none']</value>
      <webElementGuid>7eefb125-0131-4ef2-b3d2-eb433fcf9b64</webElementGuid>
   </webElementProperties>
</WebElementEntity>
