<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LabelinvalidEmailOrPasswrd</name>
   <tag></tag>
   <elementGuidId>c45dcee1-9eed-425c-a3b3-e708ead0be6f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='alert alert-danger alert-dismissible position-fixed top-0 start-50 translate-middle-x mt-5']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='alert alert-danger alert-dismissible position-fixed top-0 start-50 translate-middle-x mt-5']</value>
      <webElementGuid>53ea859d-9774-408e-af85-60958b9df22c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
