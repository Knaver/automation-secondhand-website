<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FieldPassword</name>
   <tag></tag>
   <elementGuidId>cd72bb32-ec0a-4fa1-9412-d76b39c16b53</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='user_password']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='user_password']</value>
      <webElementGuid>25d3eac7-c6e2-4c10-97b1-37d564b8afc6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
