<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Icon_Profil</name>
   <tag></tag>
   <elementGuidId>7443cd88-051e-46ba-9f2e-577d6bc6c762</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'bi bi-person me-4 me-lg-0')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>bi bi-person me-4 me-lg-0</value>
      <webElementGuid>237946f0-eb41-4fd0-ad9e-3dac0676f07b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
