<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>IconProfile</name>
   <tag></tag>
   <elementGuidId>a36c11fa-0406-47a7-9ade-299b29e5cac2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[@class='nav-item dropdown fs-5 d-none d-lg-block']//i[@class='bi bi-person me-4 me-lg-0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//li[@class='nav-item dropdown fs-5 d-none d-lg-block']//i[@class='bi bi-person me-4 me-lg-0']</value>
      <webElementGuid>72591001-17f7-4174-83a9-7fa8afe7acc6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
