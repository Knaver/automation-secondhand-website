<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LabelNameProfile</name>
   <tag></tag>
   <elementGuidId>596cdd77-42cb-464b-858a-b08452dbea41</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='fs-5 fw-bold']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='fs-5 fw-bold']</value>
      <webElementGuid>e77c85ec-b48a-429f-a6db-2b08eed1ebd2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
