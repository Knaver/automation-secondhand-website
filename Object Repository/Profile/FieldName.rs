<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FieldName</name>
   <tag></tag>
   <elementGuidId>1c36e20e-f863-4ef3-9d22-8b0200a67808</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='user_name']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='user_name']</value>
      <webElementGuid>4e095e99-b8e6-4567-b5a4-d3a89c6b2358</webElementGuid>
   </webElementProperties>
</WebElementEntity>
