<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnNego</name>
   <tag></tag>
   <elementGuidId>1673f7f0-2dcd-4e41-ab8e-f0d52a0f32d8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class='btn btn-lg btn-primary fs-6 rounded-4 w-100 mb-3 py-3 fw-bold']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class='btn btn-lg btn-primary fs-6 rounded-4 w-100 mb-3 py-3 fw-bold']</value>
      <webElementGuid>edd0e593-a388-4eaa-b099-cda812afe336</webElementGuid>
   </webElementProperties>
</WebElementEntity>
