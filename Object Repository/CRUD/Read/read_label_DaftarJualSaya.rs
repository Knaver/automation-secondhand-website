<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>read_label_DaftarJualSaya</name>
   <tag></tag>
   <elementGuidId>f00dcb01-66f0-401b-b891-bbe6adce9cc4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h3.fw-bold.my-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Bandung'])[1]/following::h3[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>01ddf417-7c00-4401-b62e-504ae39034b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fw-bold my-5</value>
      <webElementGuid>445e2628-e651-4205-8c0b-4bb93a2ae3f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Daftar Jual Saya</value>
      <webElementGuid>48f25300-6642-4a4a-81d5-c11e3239685c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/h3[@class=&quot;fw-bold my-5&quot;]</value>
      <webElementGuid>f24add3f-cb3d-49b0-a4e3-3a9ec8ff2108</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bandung'])[1]/following::h3[1]</value>
      <webElementGuid>9a2b5a7f-b741-4735-8986-f04d723da017</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Groot'])[2]/preceding::h3[1]</value>
      <webElementGuid>259a3382-849d-4223-9e76-e972e3cf16ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bandung'])[2]/preceding::h3[1]</value>
      <webElementGuid>34d01b8d-deb3-4840-9a55-7bd1bcdd030b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar Jual Saya']/parent::*</value>
      <webElementGuid>58f1edb8-be05-4b76-9343-d9e7db986e34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
      <webElementGuid>cabcd0aa-a11b-4947-a4a5-b3a539cd07cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Daftar Jual Saya' or . = 'Daftar Jual Saya')]</value>
      <webElementGuid>ee47f7c8-d40b-466a-8292-bee0a1e5f88f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
