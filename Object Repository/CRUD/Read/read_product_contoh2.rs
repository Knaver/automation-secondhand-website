<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>read_product_contoh2</name>
   <tag></tag>
   <elementGuidId>1125a12c-aaa5-4926-a8da-9b9e42879944</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 80.000'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>16a5b996-a14f-463e-8dd1-74fb3b6805a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card px-0 border-0 shadow h-100 pb-4 rounded-4</value>
      <webElementGuid>e90a5b66-62a8-4306-af8b-b1165eec47c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      

    
      Aeon
      Hobi
      Rp 2.500.000
    
  </value>
      <webElementGuid>24bbdb65-c0fa-4fb7-9675-13f8e99bbe67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-9 mt-5 mt-lg-0&quot;]/div[@class=&quot;row g-4&quot;]/div[@class=&quot;col-12 col-lg-4&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]</value>
      <webElementGuid>956fe56e-c91a-49d6-a95a-f2ddd220bcb0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 80.000'])[1]/following::div[2]</value>
      <webElementGuid>81fff371-6076-430f-8a39-f8af0e28da25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mainan Ungu mk.22'])[1]/following::div[2]</value>
      <webElementGuid>c6aeb017-e1c9-40f7-b0c4-32ffd2f774c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/a/div</value>
      <webElementGuid>1d0c6cbf-8c2e-41c2-b2fe-a3d08c228b12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      

    
      Aeon
      Hobi
      Rp 2.500.000
    
  ' or . = '
      

    
      Aeon
      Hobi
      Rp 2.500.000
    
  ')]</value>
      <webElementGuid>bcaf9fec-ec47-4d9d-a145-9e8aa0a70cb4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
