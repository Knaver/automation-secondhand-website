<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>update_choose_product1</name>
   <tag></tag>
   <elementGuidId>148dfd6e-63aa-4ae1-859d-b6924f303055</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[4]//div[@class='card px-0 border-0 shadow h-100 pb-4 rounded-4']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>86f2cd12-d2df-4462-bd19-b95ef4928d0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card px-0 border-0 shadow h-100 pb-4 rounded-4</value>
      <webElementGuid>39228b24-fae0-4966-9e1a-e99840096112</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      

    
      Testing pake sambel
      Hobi
      Rp 50.000
    
  </value>
      <webElementGuid>82633e45-ed5c-46f8-aea6-14070f28b669</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[4]//div[@class='card px-0 border-0 shadow h-100 pb-4 rounded-4']</value>
      <webElementGuid>5c91d286-eb8f-460f-964d-51d94e55ccf3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 10.000'])[1]/following::div[2]</value>
      <webElementGuid>b02a1db3-e5a4-46a6-b88f-5044f94a5428</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Baby Groot'])[1]/following::div[2]</value>
      <webElementGuid>62e454ae-8cc7-49fc-970c-df16b330c23a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/a/div</value>
      <webElementGuid>a6926935-fda3-4d44-8617-b1e28ac1fa4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      

    
      Testing pake sambel
      Hobi
      Rp 50.000
    
  ' or . = '
      

    
      Testing pake sambel
      Hobi
      Rp 50.000
    
  ')]</value>
      <webElementGuid>c2649cdc-bbde-46e7-a4c4-8de433ef038b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
