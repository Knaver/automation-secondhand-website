<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Create_card_product</name>
   <tag></tag>
   <elementGuidId>8ce97069-550b-4a2f-8d3b-3a8b2a5ca197</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card.p-2.rounded-4.shadow.border-0 > div.card-body</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='card p-2 rounded-4 shadow border-0']/div[@class='card-body']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>106c3239-f99a-49c0-bb47-951ffa7d7c10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-body</value>
      <webElementGuid>28652eb2-c143-4397-a47b-3a4ad73ffb43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
          Combo Kenyang
          Hobi
          Rp 125.000
            Edit
            Delete
        </value>
      <webElementGuid>ad93c017-bfef-4936-a2c6-b9bfbb488143</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container my-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-4&quot;]/div[@class=&quot;card p-2 rounded-4 shadow border-0&quot;]/div[@class=&quot;card-body&quot;]</value>
      <webElementGuid>b657a4bc-0702-4991-9fc3-d192569584ff</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::div[3]</value>
      <webElementGuid>ec98c387-151c-41fc-8fe8-6bcc67858daf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Product was successfully created.'])[1]/following::div[10]</value>
      <webElementGuid>3d194b3a-c7f7-41fb-83ad-f43d5c92430d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div</value>
      <webElementGuid>0a27410b-1f93-4e27-992a-3dbc2f34cae2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
          Combo Kenyang
          Hobi
          Rp 125.000
            Edit
            Delete
        ' or . = '
          Combo Kenyang
          Hobi
          Rp 125.000
            Edit
            Delete
        ')]</value>
      <webElementGuid>caa2a2fc-0e0a-430d-ae30-6ad2a67cd6f6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
