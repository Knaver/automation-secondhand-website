<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Create_Card_Seller</name>
   <tag></tag>
   <elementGuidId>c8c9b672-9288-46b5-b5ff-c4d6f7286a96</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card.py-2.px-4.rounded-4.shadow.border-0.mt-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='card py-2 px-4 rounded-4 shadow border-0 mt-5']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d787584b-3f25-4378-864f-5e7ef1438903</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card py-2 px-4 rounded-4 shadow border-0 mt-5</value>
      <webElementGuid>237ff8d1-cccc-4ff9-b52a-3a50b75a5878</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
          

          
            Groot
            Bandung
          
        
      </value>
      <webElementGuid>cfa6a829-8e59-4428-9fed-e2dbb0666a20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container my-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-4&quot;]/div[@class=&quot;card py-2 px-4 rounded-4 shadow border-0 mt-5&quot;]</value>
      <webElementGuid>e7655ea1-b856-4261-8d88-aabbaf42d70d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[1]/following::div[1]</value>
      <webElementGuid>b0a38603-e641-461a-8b41-773dcacaaab8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::div[1]</value>
      <webElementGuid>c2ce810a-ea24-4591-96cd-bf4641b7bab4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div[2]</value>
      <webElementGuid>39d0a4eb-d64e-4cdc-b47b-7b2f147e0bba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        
          

          
            Groot
            Bandung
          
        
      ' or . = '
        
          

          
            Groot
            Bandung
          
        
      ')]</value>
      <webElementGuid>8370d411-10cb-4bf4-a358-52577df0bb6f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
