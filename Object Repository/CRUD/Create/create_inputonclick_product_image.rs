<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>create_inputonclick_product_image</name>
   <tag></tag>
   <elementGuidId>3937abdb-5921-4c2c-bf41-ef233345b1fd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.form-image.rounded-4.img-preview</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@onclick='clickFileInput(this)']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;input-images&quot;)/div[@class=&quot;form-image rounded-4 img-preview&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e6c5427a-f0f1-4ab9-8f60-5a541e5871df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-image rounded-4 img-preview</value>
      <webElementGuid>8a942b89-7505-41de-b53f-300c0880efb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>clickFileInput(this)</value>
      <webElementGuid>e07afb88-7ab9-4cd1-8487-be8fc06d408d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;input-images&quot;)/div[@class=&quot;form-image rounded-4 img-preview&quot;]</value>
      <webElementGuid>2fa81dbe-d3da-412f-b20d-373cd21d99dd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@onclick='clickFileInput(this)']</value>
      <webElementGuid>2f0b7941-d184-4391-8869-d9dd3ed8c351</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='input-images']/div</value>
      <webElementGuid>43f07afa-5456-4321-b845-d722a6b886b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::div[2]</value>
      <webElementGuid>06f25985-d81e-400f-8d44-0392059efc9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::div[3]</value>
      <webElementGuid>ad526d8a-90e4-4b5b-afef-aa4eb043812c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[1]/preceding::div[1]</value>
      <webElementGuid>c47e212a-c525-4721-b3fc-c98e9e929f4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terbitkan'])[1]/preceding::div[1]</value>
      <webElementGuid>27d65e5f-6727-413b-93d0-dd86898d7eb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div</value>
      <webElementGuid>0276df55-7032-4617-a0f3-e19ff8088fff</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
