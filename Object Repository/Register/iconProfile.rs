<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iconProfile</name>
   <tag></tag>
   <elementGuidId>ef42b2e1-f82a-4f8f-a768-c042ab1b4fea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[@class='nav-item dropdown fs-5 d-none d-lg-block']//i[@class='bi bi-person me-4 me-lg-0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//li[@class='nav-item dropdown fs-5 d-none d-lg-block']//i[@class='bi bi-person me-4 me-lg-0']</value>
      <webElementGuid>fc751a1e-7fc8-4c3b-a91b-8d477a263dff</webElementGuid>
   </webElementProperties>
</WebElementEntity>
