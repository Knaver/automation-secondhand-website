<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>fieldEmail</name>
   <tag></tag>
   <elementGuidId>bfe5fa33-3bf9-4de7-a886-0a48262b3bf7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='user_email']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='user_email']</value>
      <webElementGuid>57d98616-b12d-4afb-a4c6-cdfa943d1725</webElementGuid>
   </webElementProperties>
</WebElementEntity>
