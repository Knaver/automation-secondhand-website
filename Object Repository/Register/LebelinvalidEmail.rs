<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LebelinvalidEmail</name>
   <tag></tag>
   <elementGuidId>c6f88464-7bb8-4b7e-8653-af112b4c2ca1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='form-text text-danger']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='form-text text-danger']</value>
      <webElementGuid>0c792be8-74f3-4933-a5a4-cad6c5f09df5</webElementGuid>
   </webElementProperties>
</WebElementEntity>
