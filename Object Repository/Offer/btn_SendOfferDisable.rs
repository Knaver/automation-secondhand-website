<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_SendOfferDisable</name>
   <tag></tag>
   <elementGuidId>ee626c19-9e47-48bd-b17a-3b0cf45858fc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'btn btn-lg btn-primary fs-6 rounded-4 w-100 mb-3 py-3 fw-bold disabled')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='btn btn-lg btn-primary fs-6 rounded-4 w-100 mb-3 py-3 fw-bold disabled']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-lg btn-primary fs-6 rounded-4 w-100 mb-3 py-3 fw-bold disabled</value>
      <webElementGuid>0e55df47-c91b-4d2f-8d2c-c35f91c0a94e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
