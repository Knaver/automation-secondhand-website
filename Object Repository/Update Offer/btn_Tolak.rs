<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Tolak</name>
   <tag></tag>
   <elementGuidId>aed5ed28-644a-4ae3-b8f6-cf6cc0e80bac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>.btn-outline-primary</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = '.btn-outline-primary']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-outline-primary rounded-4 px-5</value>
      <webElementGuid>263f5eb3-91a7-4a1a-8a2a-2fb9d6b87720</webElementGuid>
   </webElementProperties>
</WebElementEntity>
