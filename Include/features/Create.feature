@create
Feature: Create

	@CRT001
	Scenario: Seller creates valid product
		Given Seller in landing page
		When Seller click + Jual button
		And Seller input product name
		And Seller input product price
		And Seller select product category
		And Seller input product description
		And Seller upload product image
		And Seller click Terbitkan button
		Then Seller redirected to created product page
		
	@CRT002
	Scenario: Seller creates product with empty informations
		Given Seller in landing page
		When Seller click + Jual button
		And Seller click Terbitkan button
		Then Website notify seller that all product informations are empty
		
	@CRT003
	Scenario: Seller creates product with empty name
		Given Seller in landing page
		When Seller click + Jual button
		And Seller input product price
		And Seller select product category
		And Seller input product description
		And Seller upload product image
		And Seller click Terbitkan button
		Then Website notify seller that product name is empty
		
	@CRT004
	Scenario: Seller creates product with empty price
		Given Seller in landing page
		When Seller click + Jual button
		And Seller input product name
		And Seller select product category
		And Seller input product description
		And Seller upload product image
		And Seller click Terbitkan button
		Then Website notify seller that product price is empty
		
	@CRT005
	Scenario: Seller creates product with empty category
		Given Seller in landing page
		When Seller click + Jual button
		And Seller input product name
		And Seller input product price
		And Seller input product description
		And Seller upload product image
		And Seller click Terbitkan button
		Then Website notify seller that product category is empty
		
	@CRT006
	Scenario: Seller creates product with empty description
		Given Seller in landing page
		When Seller click + Jual button
		And Seller input product name
		And Seller input product price
		And Seller select product category
		And Seller upload product image
		And Seller click Terbitkan button
		Then Website notify seller that product description is empty
		
	@CRT007
	Scenario: Seller creates product with empty image
		Given Seller in landing page
		When Seller click + Jual button
		And Seller input product name
		And Seller input product price
		And Seller select product category
		And Seller input product description
		And Seller click Terbitkan button
		Then Website notify seller that product image is empty