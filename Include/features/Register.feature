@Register
Feature: Register

@REG001
Scenario: Buyer success register with valid credential
Given User success landing on register page
When Buyer input Nama
And Buyer input valid Email
And Buyer input Password
Then User success register account and redirect to homepage


@REG002 
Scenario: Seller success register with valid credential 
Given User success landing on register page 
When Seller input Nama 
And Seller input valid Email
And Seller input Password
Then User success register account and redirect to homepage


@REG003 
Scenario: User failed register account with invalid Email
Given User success landing on register page
When Buyer input Nama
And User input invalid Email
And Buyer input Password
Then User failed register with invalid Email