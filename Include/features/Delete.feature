@delete
Feature: Delete

	@DEL001
	Scenario: Seller delete one of their products
	Given Seller in product list page
	When Seller chooses a product
	And Seller click Delete button
	Then Seller reach product list page