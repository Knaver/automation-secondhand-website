@Profile
Feature: Profile

  @P001
  Scenario: User success update avatar profile
    Given user already login with valid data
    When user click icon Profile
    And user click label nama profile
    And user edit avatar profile
    Then user click simpan button

  @P002
  Scenario: User success update city
    Given user already login with valid data
    When user click icon Profile
    And user click label nama profile
    And user edit city
    Then user click simpan button

  @P003
  Scenario: User success update Full address
    Given user already login with valid data
    When user click icon Profile
    And user click label nama profile
    And user edit full address
    Then user click simpan button

  @P004
  Scenario: User success update phone number
    Given user already login with valid data
    When user click icon Profile
    And user click label nama profile
    And user edit phone number
    Then user click simpan button

  @P005
  Scenario: User success update profile name
    Given user already login with valid data
    When user click icon Profile
    And user click label nama profile
    And user edit profile name
    Then user click simpan button
    
   @P006
  Scenario: User success update akun
    Given user already login with valid data
    When user click icon Profile
    And user click label nama profile
    And user edit avatar profile
    And user edit city
    And user edit full address
    And user edit phone number
    And user edit profile name
    Then user click simpan button
