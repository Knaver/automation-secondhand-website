@offer
Feature: Offer

  @OF001
  Scenario: buyer want to send an offer to the seller
    Given buyer already in homepage
    And login successfully
    And search product
    When buyer chooses a product to buy
    And buyer clicks the button, I'm interested and want to negotiate
    And buyer inputs the bid price field
    And buyer clicks the send button
    Then button changes to disable with the label waiting for the seller's response

  @OF002
  Scenario: buyer want to send an offer without login to the seller
    Given already in homepage
    And search product
    When buyer chooses a product to buy
    And buyer clicks the button, I'm interested and want to negotiate
    Then the login process form appears

  @OF004
  Scenario: seller want to accept buyer offer
    Given seller already in homepage
    And login successfully
    When seller click notification icon
    And seller click hyperlink Lihat semua notifikasi
    And seller choose buyer offer
    And seller click accept button
    Then Product status changed successfully to Accepted

  @OF005
  Scenario: seller want to decline buyer offer
    Given seller already in homepage
    And login successfully
    When seller click notification icon
    And seller click hyperlink Lihat semua notifikasi
    And seller choose buyer offer
    And seller click reject button
    Then Product status changed successfully to Rejected
