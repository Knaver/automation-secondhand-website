@product
Feature: product

  @PRO001
  Scenario: User success search product
    Given user already login
    When user click field search
    And user edit field search
    And user click product
    Then user search product successfully

  @PRO002
  Scenario: User success search product by category
    Given user already login
    When user click field search
    And user edit field search
    And click category button
    And user click product by category
    Then user search product successfully
