@login
Feature: Login

  @LG001
  Scenario: User failed login with invalid data
    Given user click Masuk button
    When user Input wrong email
    And user input wrong password
    Then user failed to login using invalid data

  @LG002
  Scenario: Buyer login with valid credential
    Given buyer Input correct email
    And buyer Input correct password
    Then user click login button
  
  @LG003
  Scenario: Seller login with valid credential
    Given user click Masuk button
    When seller Input correct email
    And seller Input correct password
    Then user click login button
    
