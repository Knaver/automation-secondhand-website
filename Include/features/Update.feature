@update
Feature: Update

	@UPD001
	Scenario: Seller update product with valid informations
	Given Seller in product list page
	When Seller chooses a product
	And Seller click Edit button
	And Seller update product name
	And Seller update product price
	And Seller update product category
	And Seller update product description
	And Seller update product image
	And Seller click Terbitkan button
	Then Seller redirected to created product page
	
	@UPD002
	Scenario: Seller update product with empty informations
	Given Seller in product list page
	When Seller chooses a product
	And Seller click Edit button
	And Seller blanks product name
	And Seller blanks product price
	And Seller blanks product category
	And Seller blanks product description
	And Seller blanks product image
	And Seller click Terbitkan button
	Then Website notify seller that all product informations are empty
	
	@UPD003
	Scenario: Seller update product with empty name
	Given Seller in product list page
	When Seller chooses a product
	And Seller click Edit button
	And Seller blanks product name
	And Seller update product price
	And Seller update product category
	And Seller update product description
	And Seller update product image
	And Seller click Terbitkan button
	Then Website notify seller that product name is empty
	
	@UPD004
	Scenario: Seller update product with empty price
	Given Seller in product list page
	When Seller chooses a product
	And Seller click Edit button
	And Seller update product name
	And Seller blanks product price
	And Seller update product category
	And Seller update product description
	And Seller update product image
	And Seller click Terbitkan button
	Then Website notify seller that product price is empty
	
	@UPD005
	Scenario: Seller update product with empty category
	Given Seller in product list page
	When Seller chooses a product
	And Seller click Edit button
	And Seller update product name
	And Seller update product price
	And Seller blanks product category
	And Seller update product description
	And Seller update product image
	And Seller click Terbitkan button
	Then Website notify seller that product category is empty
	
	@UPD006
	Scenario: Seller update product with empty description
	Given Seller in product list page
	When Seller chooses a product
	And Seller click Edit button
	And Seller update product name
	And Seller update product price
	And Seller update product category
	And Seller blanks product description
	And Seller update product image
	And Seller click Terbitkan button
	Then Website notify seller that product description is empty
	
	@UPD007
	Scenario: Seller update product with empty image
	Given Seller in product list page
	When Seller chooses a product
	And Seller click Edit button
	And Seller update product name
	And Seller update product price
	And Seller update product category
	And Seller update product description
	And Seller blanks product image
	And Seller click Terbitkan button
	Then Website notify seller that product image is empty
	