package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Create {

	@Given("Seller in landing page")
	public void seller_in_landing_page() {
		WebUI.callTestCase(findTestCase('Login/Step Definition/User click masuk button'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Login/Step Definition/Seller input correct email'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Login/Step Definition/Seller input correct password'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Login/Step Definition/User click button login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Seller click + Jual button")
	public void seller_click_Jual_button() {
		WebUI.callTestCase(findTestCase('CRUD/Create/Step Definition/User enter create product page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Seller input product name")
	public void seller_input_product_name() {
		WebUI.callTestCase(findTestCase('CRUD/Create/Step Definition/User input product name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Seller input product price")
	public void seller_input_product_price() {
		WebUI.callTestCase(findTestCase('CRUD/Create/Step Definition/User input product price'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Seller select product category")
	public void seller_select_product_category() {
		WebUI.callTestCase(findTestCase('CRUD/Create/Step Definition/User select product category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Seller input product description")
	public void seller_input_product_description() {
		WebUI.callTestCase(findTestCase('CRUD/Create/Step Definition/User input product description'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Seller upload product image")
	public void seller_upload_product_image() {
		WebUI.callTestCase(findTestCase('CRUD/Create/Step Definition/User upload product image'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Seller click Terbitkan button")
	public void seller_click_Terbitkan_button() {
		WebUI.callTestCase(findTestCase('CRUD/Create/Step Definition/User press Terbitkan button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Seller redirected to created product page")
	public void seller_redirected_to_created_product_page() {
		WebUI.callTestCase(findTestCase('CRUD/Create/Step Definition/Verify product is created'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.verifyElementPresent(findTestObject('Profile/IconProfile'), 3)

		WebUI.click(findTestObject('Profile/IconProfile'))

		WebUI.verifyElementPresent(findTestObject('Logout/Logout'), 3)

		WebUI.click(findTestObject('Logout/Logout'))
	}


	@Then("Website notify seller that all product informations are empty")
	public void website_notify_seller_that_all_product_informations_are_empty() {
		WebUI.verifyElementPresent(findTestObject('CRUD/Create/create_message_blank_name'), 0)
		WebUI.verifyElementPresent(findTestObject('CRUD/Create/create_message_blank_price'), 0)
		WebUI.verifyElementPresent(findTestObject('CRUD/Create/create_message_blank_category'), 0)
		WebUI.verifyElementPresent(findTestObject('CRUD/Create/create_message_blank_description'), 0)
	}

	@Then("Website notify seller that product name is empty")
	public void website_notify_seller_that_product_name_is_empty() {
		WebUI.verifyElementPresent(findTestObject('CRUD/Create/create_message_blank_name'), 0)
	}

	@Then("Website notify seller that product price is empty")
	public void website_notify_seller_that_product_price_is_empty() {
		WebUI.verifyElementPresent(findTestObject('CRUD/Create/create_message_blank_price'), 0)
	}

	@Then("Website notify seller that product category is empty")
	public void website_notify_seller_that_product_category_is_empty() {
		WebUI.verifyElementPresent(findTestObject('CRUD/Create/create_message_blank_category'), 0)
	}

	@Then("Website notify seller that product description is empty")
	public void website_notify_seller_that_product_description_is_empty() {
		WebUI.verifyElementPresent(findTestObject('CRUD/Create/create_message_blank_description'), 0)
	}

	@Then("Website notify seller that product image is empty")
	public void website_notify_seller_that_product_image_is_empty() {
	}
}
