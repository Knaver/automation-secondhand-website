package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Profile {
	@Given("user already login with valid data")
	public void user_already_login_with_valid_data() {
		WebUI.callTestCase(findTestCase('Login/Pages/LG004- User success login with valid data'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user click icon Profile")
	public void user_click_icon_Profile() {
		WebUI.callTestCase(findTestCase('Profile/Step Definition/User click Icon Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user click label nama profile")
	public void user_click_label_nama_profile() {
		WebUI.callTestCase(findTestCase('Profile/Step Definition/User click label nama profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user edit avatar profile")
	public void user_edit_avatar_profile() {
		WebUI.callTestCase(findTestCase('Profile/Step Definition/User edit avatar profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}


	@When("user edit city")
	public void user_edit_city() {
		WebUI.callTestCase(findTestCase('Profile/Step Definition/User edit city'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user edit full address")
	public void user_edit_full_address() {
		WebUI.callTestCase(findTestCase('Profile/Step Definition/User edit full address'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user edit phone number")
	public void user_edit_phone_number() {
		WebUI.callTestCase(findTestCase('Profile/Step Definition/User edit phone number'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user edit profile name")
	public void user_edit_profile_name() {
		WebUI.callTestCase(findTestCase('Profile/Step Definition/User edit profile name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click simpan button")
	public void user_click_simpan_button() {
		WebUI.callTestCase(findTestCase('Profile/Step Definition/User click simpan button'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Logout/Step Definition/User Logout'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}