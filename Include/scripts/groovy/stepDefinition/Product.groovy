package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Product {

	@Given("user already login")
	public void user_already_login() {
		WebUI.callTestCase(findTestCase('Login/Pages/LG002- Buyer success login with valid credential'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user click field search")
	public void user_click_field_search() {
		WebUI.callTestCase(findTestCase('Product/Step Definition/user click field search'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("user edit field search")
	public void user_edit_field_search() {
		WebUI.callTestCase(findTestCase('Product/Step Definition/user input field search'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click product")
	public void user_click_product() {
		WebUI.callTestCase(findTestCase('Product/Step Definition/user click product'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("click category button")
	public void click_category_button() {
		WebUI.callTestCase(findTestCase('Product/Step Definition/user click button category'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("user click product by category")
	public void user_click_product_by_category() {
		WebUI.callTestCase(findTestCase('Product/Step Definition/user click product by category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user search product successfully")
	public void user_search_product_successfully() {
		WebUI.callTestCase(findTestCase('Product/Step Definition/user success search product'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}