package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Offer {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("seller already in homepage")
	public void seller_already_in_homepage() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.callTestCase(findTestCase('Offer/Step Definition/user login as a seller'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("already in homepage")
	public void already_in_homepage() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.verifyElementPresent(findTestObject('Login/btnMasuk'), 2)
	}
	
	@Given("buyer already in homepage")
	public void buyer_already_in_homepage() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.callTestCase(findTestCase('Offer/Step Definition/user login as a buyer'), [:], FailureHandling.STOP_ON_FAILURE)
	}


	@And("login successfully")
	public void login_successfully() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.verifyElementPresent(findTestObject('Login/Icon_Profil'), 2)
	}

	@When("seller click notification icon")
	public void seller_click_notification_icon() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.click(findTestObject('Update Offer/btn_Notif'))
	}

	@And("seller click hyperlink Lihat semua notifikasi")
	public void seller_click_hyperlink_Lihat_semua_notifikasi() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.click(findTestObject('Update Offer/hyperlink_LihatSemua'))
	}

	@And("seller choose buyer offer")
	public void user_choose_buyer_offer() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.click(findTestObject('Update Offer/hyperlink_Notif'))
	}

	@And("seller click reject button")
	public void seller_click_reject_button() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.click(findTestObject('Update Offer/btn_Tolak'),  [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@And("seller click accept button")
	public void user_click_accept_button() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.click(findTestObject('Update Offer/btn_terima'),  [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("Product status changed successfully to Rejected")
	public void product_status_changed_successfully_to_Rejected() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.verifyTextPresent('Penawaran produk ditolak', false)
	}

	@And("search product")
	public void search_product() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.callTestCase(findTestCase('Product/Step Definition/user click field search'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Product/Step Definition/user input field search'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.sendKeys(findTestObject('Product/fieldSearchProd'), '')
	}

	@When("buyer chooses a product to buy")
	public void buyer_chooses_a_product_to_buy() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.click(findTestObject('Offer/crd_product'))
	}

	@And("buyer clicks the button, I'm interested and want to negotiate")
	public void buyer_clicks_the_button_I_m_interested_and_want_to_negotiate() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.click(findTestObject('Offer/btn_SayaTertarik'))
	}

	@Then("the login process form appears")
	public void the_login_process_form_appears() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.verifyElementPresent(findTestObject('Login/FieldEmail'), 2)
	}

	@Then("Product status changed successfully to Accepted")
	public void product_status_changed_successfully_to_Accepted() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.verifyTextPresent('Penawaran produk diteima', false)
	}

	@And("buyer inputs the bid price field")
	public void buyer_inputs_the_bid_price_field() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.setText(findTestObject('Offer/tf_OfferPrice'), '50000')
	}

	@And("buyer clicks the send button")
	public void buyer_clicks_the_send_button() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.click(findTestObject('Offer/btn_SendOffer'))
	}

	@Then("button changes to disable with the label waiting for the seller's response")
	public void button_changes_to_disable_with_the_label_waiting_for_the_seller_s_response() {
		// Write code here that turns the phrase above into concrete actions
		WebUI.verifyElementPresent(findTestObject('Offer/btn_SendOffer'), 2)
	}
}