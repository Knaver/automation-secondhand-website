package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Login {
	@Given("user click Masuk button")
	public void user_click_Masuk_button() {
		WebUI.callTestCase(findTestCase('Login/Step Definition/User click masuk button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user Input wrong email")
	public void user_Input_wrong_email() {
		WebUI.callTestCase(findTestCase('Login/Step Definition/User input wrong email'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input wrong password")
	public void user_input_wrong_password() {
		WebUI.callTestCase(findTestCase('Login/Step Definition/User input wrong password'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user failed to login using invalid data")
	public void user_failed_to_login_using_invalid_data() {
		WebUI.callTestCase(findTestCase('Login/Step Definition/User failed login with invalid Email Or Paswrd'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("buyer Input correct email")
	public void buyer_Input_correct_email() {
		WebUI.callTestCase(findTestCase('Login/Step Definition/Buyer input correct email'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("buyer Input correct password")
	public void buyer_Input_correct_password() {
		WebUI.callTestCase(findTestCase('Login/Step Definition/Buyer input correct password'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click login button")
	public void user_click_login_button() {
		WebUI.callTestCase(findTestCase('Login/Step Definition/User click button login'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Logout/Step Definition/User Logout'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("seller Input correct email")
	public void seller_Input_correct_email() {
		WebUI.callTestCase(findTestCase('Login/Step Definition/Seller input correct email'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("seller Input correct password")
	public void seller_Input_correct_password() {
		WebUI.callTestCase(findTestCase('Login/Step Definition/Seller input correct password'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}