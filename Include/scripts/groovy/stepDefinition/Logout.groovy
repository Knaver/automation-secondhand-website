package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import internal.GlobalVariable

public class Logout {
	@Given("User success login with valid data")
	public void user_success_login_with_valid_data() {
		WebUI.callTestCase(findTestCase('Login/Step Definition/User click masuk button'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Login/Step Definition/Seller input correct email'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Login/Step Definition/Seller input correct password'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Login/Step Definition/User click button login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User Logout")
	public void user_Logout() {
		WebUI.verifyElementPresent(findTestObject('Profile/IconProfile'), 3)

		WebUI.click(findTestObject('Profile/IconProfile'))

		WebUI.verifyElementPresent(findTestObject('Logout/Logout'), 3)

		WebUI.click(findTestObject('Logout/Logout'))
	}
}
