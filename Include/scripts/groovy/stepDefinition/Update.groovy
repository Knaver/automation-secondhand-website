package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Update {
	
	@And("Seller click Edit button")
	public void seller_click_Edit_button() {
		WebUI.click(findTestObject('CRUD/Update/update_button_editproduct'))

	}
	
	@And("Seller update product name")
	public void seller_update_product_name() {
		WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User update product name'), [:], FailureHandling.STOP_ON_FAILURE)

	}
	
	@And("Seller update product price")
	public void seller_update_product_price() {
		WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User update product price'), [:], FailureHandling.STOP_ON_FAILURE)

	}
	
	@And("Seller update product category")
	public void seller_update_product_category() {
		WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User update product category'), [:], FailureHandling.STOP_ON_FAILURE)

	}
	
	@And("Seller update product description")
	public void seller_update_product_description() {
		WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User update product description'), [:], FailureHandling.STOP_ON_FAILURE)

	}
	
	@And("Seller update product image")
	public void seller_update_product_image() {
		WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User update product image'), [:], FailureHandling.STOP_ON_FAILURE)

	}
	
	@And("Seller blanks product name")
	public void seller_blanks_product_name() {
		WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User clear product name'), [:], FailureHandling.STOP_ON_FAILURE)

	}
	
	@And("Seller blanks product price")
	public void seller_blanks_product_price() {
		WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User clear product price'), [:], FailureHandling.STOP_ON_FAILURE)

	}
	
	@And("Seller blanks product category")
	public void seller_blanks_product_category() {
		WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User clear product category'), [:], FailureHandling.STOP_ON_FAILURE)

	}
	
	@And("Seller blanks product description")
	public void seller_blanks_product_description() {
		WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User clear product description'), [:], FailureHandling.STOP_ON_FAILURE)

	}
	
	@And("Seller blanks product image")
	public void seller_blanks_product_image() {
		WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User clear product old image'), [:], FailureHandling.STOP_ON_FAILURE)

	}
}
