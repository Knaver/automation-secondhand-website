package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Register {
	@Given("User success landing on register page")
	public void user_success_landing_on_register_page() {
		WebUI.callTestCase(findTestCase('Register/Step Definition/User success landing on register page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Buyer input Nama")
	public void buyer_input_Nama() {
		WebUI.callTestCase(findTestCase('Register/Step Definition/Buyer input Nama'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Buyer input valid Email")
	public void buyer_input_valid_Email() {
		WebUI.callTestCase(findTestCase('Register/Step Definition/Buyer input valid Email'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Buyer input Password")
	public void buyer_input_Password() {
		WebUI.callTestCase(findTestCase('Register/Step Definition/Buyer input password'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User success register account and redirect to homepage")
	public void user_success_register_account_and_redirect_to_homepage() {
		WebUI.callTestCase(findTestCase('Register/Step Definition/User success register account and redirect to homepage'), [:],
		FailureHandling.STOP_ON_FAILURE)
	}

	@And("Seller input Nama")
	public void seller_input_Nama() {
		WebUI.callTestCase(findTestCase('Register/Step Definition/Seller input Nama'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Seller input valid Email")
	public void seller_input_valid_Email() {
		WebUI.callTestCase(findTestCase('Register/Step Definition/Seller input valid Email'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Seller input Password")
	public void seller_input_Password() {
		WebUI.callTestCase(findTestCase('Register/Step Definition/Seller input Password'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User input invalid Email")
	public void user_input_invalid_Email() {
		WebUI.callTestCase(findTestCase('Register/Step Definition/User input invalid Email'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User failed register with invalid Email")
	public void user_failed_register_with_invalid_Email() {
		WebUI.callTestCase(findTestCase('Register/Step Definition/User failed register with invalid Email'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
