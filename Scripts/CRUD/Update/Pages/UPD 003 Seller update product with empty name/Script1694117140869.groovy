import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User enter update product page'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User clear product name'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User update product price'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User update product category'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User update product description'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User clear product old image'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CRUD/Update/Step Definition/User upload product new image'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CRUD/Create/Step Definition/User press Terbitkan button'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('CRUD/Create/create_message_blank_name'), 0)

